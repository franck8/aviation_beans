package aviation_beans;

public class Plane {

	private String model; 
	private boolean available; 
	
	public Plane() {
		this(null, true);
	}

	public Plane(String model, boolean available) {
		this.model = model;
		this.available = available;
}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Override
	public String toString() {
		return "model = " + model + "\n\n" + "available =" + available; 
	}
	
	public void init() {
		System.out.println("init");
	}
	
	public void destroy() {
		System.out.println("destroy");
	}
}