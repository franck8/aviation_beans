package aviation_beans;

import java.time.LocalDateTime;
import java.util.Set;

public abstract class Carrier {

	private String name;
	private LocalDateTime foundation;
	private Coordinate office;
	private Set<Plane> planes;

	public Carrier() {
		this(null);
	}

	public Carrier(String name) {
		this.name = name;
	}

	
	public abstract LocalDateTime getNow(); 
	
	@Override
	public String toString() {
		return ("name = " + name + "office = " + office + "valable le " + getNow());
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getFoundation() {
		return foundation;
	}

	public void setFoundation(LocalDateTime foundation) {
		this.foundation = foundation;
	}

	public Coordinate getOffice() {
		return office;
	}

	public void setOffice(Coordinate office) {
		this.office = office;
	}

	public Set<Plane> getPlanes() {
		return planes;
	}

	public void setPlanes(Set<Plane> planes) {
		this.planes = planes;
	}
}


	
