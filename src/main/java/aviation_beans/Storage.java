package aviation_beans;

import java.util.List;
import java.util.ArrayList;

public class Storage {

	
	private List<Double> volumes; // liste des volumes des hangars

	Storage() {
		this(new ArrayList<Double>());
	}

	public Storage(List<Double> volumes) {
		this.volumes = volumes;
	}
	@Override
	public String toString() {
		return "volumes = " + volumes;
	}

	public void addVolume(double v) {
		volumes.add(v);

	}

	public int getNumber() {
		return volumes.size();
	}

	public List<Double> getVolumes() {
		return volumes;
	}

	public void setVolumes(List<Double> volumes) {
		this.volumes = volumes;
	}

}
