package aviation_beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

import fr.toulouse.greta.formation.data.fb.BalloonService;
import fr.toulouse.greta.formation.data.fb.PlanningService;

public class Start {

	public static void main(String[] args) {
		
		

		AbstractApplicationContext context = new FileSystemXmlApplicationContext("classpath:context.xml"); // generation d'un conteneur "context" pour contenir le contenu du fichier XML 

		 

																			
		Resource ad = context.getResource("classpath:ad.txt"); 											   // generation d'un conteneur "ad" pour obtenir un fichier .txt utilisant une des fonction de context.xml ?
		try {

			File adfile = ad.getFile(); 																   // generation d'un conteneur "adfile" pour l'obtention du NOM du fichier ad.txt
			BufferedReader r = new BufferedReader(new FileReader(adfile)); 								   // generation d'un conteneur "r" avec .txt ouvert, il ne manque plus que la lecture																
			String l; 																					   // creation de variable
			while ((l = r.readLine()) != null)															   // va dans le fichier contenu et si pas possible de lire - renvoit NULL
				System.out.println(l + "\n");
			r.close(); 																					   // fermeture du fichier
		} catch (Exception ex) {
			
			ex.printStackTrace();
		}
		
		

		Coordinate c1 = (Coordinate) context.getBean("towerCoord");
		String c2 = (String) context.getBean("airportName");
		Integer c3 = (Integer) context.getBean("hangarCapacity");
		Coordinate c4 = (Coordinate) context.getBean("runwayCoord");
		Storage st = (Storage) context.getBean("storage");
		Plane p1 = (Plane) context.getBean("plane");
		LocalDateTime foundation = (LocalDateTime) context.getBean("foundation");
		p1.setAvailable(false); // avion 1 non dispo
		Plane p2 = (Plane) context.getBean("plane");
		Carrier car1 = (Carrier) context.getBean("carrier1");
		
	

		// Coordinate c1 = new Coordinate(125, -56, 2500);
		// System.out.println(c1.toString()); //avec Override on peut utiliser la
		// methode ci-dessous

		System.out.println(
				"c1 ---> " + c1 + "\n\n" + c2 + " " + c3 + " " + "m2" + "\n\n" + c4 + "\n\n" + st + "\n\n" + p1);
		System.out.println("\n");
		System.out.println(foundation);
		System.out.println(p2);
		System.out.println(car1);
		PlanningService pls = (PlanningService) context.getBean("planningServices");
		pls.execute();
		BalloonService balloonService = (BalloonService) context.getBean("balloonService");
		balloonService.execute(); 
	
		}

	

}


