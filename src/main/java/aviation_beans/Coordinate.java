package aviation_beans;

import java.io.Serializable;

public class Coordinate implements Serializable {

	private static final long serialVersionUID = 1L;
	private double latitude;
	private double longitude;
	private double altitude;
	
	public Coordinate() {
		this(0, 0, 0);
}

	public Coordinate(double latitude, double longitude, double altitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	@Override
	public String toString() {
		return "lat = " + latitude + " ; " + " long = " + longitude + " ; " + " alt = " + altitude + " ; ";
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

}
