package fr.toulouse.greta.formation.data.fb;

import org.springframework.data.repository.CrudRepository;

// dedie a un certain type de data dans cette interface mais un DAO pourra etre utiise
//specialisation de l'interface avec un type d'objet inside

public interface BalloonRepository extends CrudRepository <Balloon, Integer> { // CRUD = Create ...

	Iterable<Balloon>findByName(String m);
	Iterable<Balloon>findBySizeGreaterThanOrderByName(double s);
	Iterable<Balloon>findByAltitudeGreaterThanAndAltitudeLessThanOrderBySize(double min, double max);
	
	
}
