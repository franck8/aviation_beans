package fr.toulouse.greta.formation.data.fb;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Region;

@Region("balloonRegion") // les balloons qui seront stockes proviendront de cette classe (annotation = classe donc import a faire)
@Entity 				 // Designe quelque chose qui sera stocke quelque part
						
public class Balloon {

	private Integer id;
	private String name;
	private double size;
	private double altitude;

	Balloon() {
		this(0, null, 0, 0);
	}

	public Balloon(Integer id, String name, double size, double altitude) {

		this.id = id;
		this.name = name;
		this.size = size;
		this.altitude = altitude;
	}
	// Annotation des getters
@Id // importation done from spring Data / Gemfire sait now que Id sera via getId
	public Integer getId() {  
		return id;
	}
@Column // importation done from JPA / Gemfire sait now que Id sera via getName
public String getName() {
	return name;
}
@Column // importation done from JPA / Gemfire sait now que Id sera via getSize
public double getSize() {
	return size;
}

	public void setId(Integer id) {
		this.id = id;
	}


	public void setName(String name) {
		this.name = name;
	}


	public void setSize(double size) {
		this.size = size;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	@Override
	public String toString() {
		return ("id=" + id + "\n" + "name=" + name + "\n" + "size=" + size + "\n" + "altitude=" + altitude + "\n");
	}

}