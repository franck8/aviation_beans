package fr.toulouse.greta.formation.data.fb;

import java.time.LocalDateTime;

public class Flight {

private Long id; // pourquoi un Long ? pouvoir creer un vol avec une valeur Null 
private LocalDateTime departure;
private String origin; 
private String destination; 
private Long plane;

Flight()
{
	this(null,null,null,null);
}

public Flight(LocalDateTime departure, String origin, String destination, Long plane) {
	this.departure = departure; 
	this.origin = origin;
	this.destination = destination; 
	this.plane = plane; 
}

public Flight(Long id,LocalDateTime departure, String origin, String destination, Long plane) {
	this.id = id; 
	this.departure = departure; 
	this.origin = origin;
	this.destination = destination; 
	this.plane = plane; 
}


public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public LocalDateTime getDeparture() {
	return departure;
}
public void setDeparture(LocalDateTime departure) {
	this.departure = departure;
}
public String getOrigin() {
	return origin;
}
public void setOrigin(String origin) {
	this.origin = origin;
}
public String getDestination() {
	return destination;
}
public void setDestination(String destination) {
	this.destination = destination;
}
public Long getPlane() {
	return plane;
}
public void setPlane(Long plane) {
	this.plane = plane;
} 

public String toString() {
	return ("id =" + id + "departure =" + departure + "origin =" + origin + "destination =" + destination + "plane =" + plane);
	
}
	
	
	
	
	
}
