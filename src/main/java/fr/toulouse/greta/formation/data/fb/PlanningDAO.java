package fr.toulouse.greta.formation.data.fb;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import aviation_beans.Plane;

public class PlanningDAO {

	private NamedParameterJdbcTemplate template;
	
	private final static String ownCode = "TLS"; // constante car c'est mon aerodrome qui ne changera pas

	public void setTemplate(NamedParameterJdbcTemplate template) {
		this.template = template;
	}

	public void init() {
		String delflight = "DELETE FROM flight";
		template.getJdbcOperations().execute(delflight);
		String delplane = "DELETE FROM plane";
		template.getJdbcOperations().execute(delplane);
	}

	public long insertPlane(String model, String immat) { // long ici et pas int car il est possible d'avoir un grnd
															// nombre de vol Id
		String insPlane = "INSERT INTO plane (model, immat) VALUES(:model,:immat)"; // (:model,:immat) = cles de la map

		MapSqlParameterSource data = new MapSqlParameterSource(); // permet la recuperation d'une donnee
		data.addValue("model", model);
		data.addValue("immat", immat);

		// prepa des donnees associees a la requete

		// Map<String,String> data = new HashMap<String,String>();
		// data.put("model", model);
		// data.put("immat", immat);

		// h = objet temporaire qui contient la cle genere
		GeneratedKeyHolder h = new GeneratedKeyHolder();

		// on utilise NamedParameterJdbcTemplate qui prend en argument la requete
		template.update(insPlane, data, h);
		return h.getKey().longValue();

	}

	public void insertFlight(Flight f) {

		String insFlight = "INSERT INTO flight (departure, origin, destination, plane) VALUES (:departure,:origin, :destination, :plane)";
		MapSqlParameterSource data = new MapSqlParameterSource(); // permet la recuperation d'une donnee
		data.addValue("departure", f.getDeparture());
		data.addValue("origin", f.getOrigin());
		data.addValue("destination", f.getDestination());
		data.addValue("plane", f.getPlane());
		GeneratedKeyHolder h = new GeneratedKeyHolder();
		template.update(insFlight, data, h);
		f.toString();

	}

	public List<Flight> getFlightsByOrigin(String origin) {
		String selFlights = "SELECT* FROM flight WHERE origin LIKE :origin"; 				// stockage de la requete dans la variable
																							// selflight
		MapSqlParameterSource data = new MapSqlParameterSource(); 							// stockage de la creation d'une map SQL dans la variable data
																						
		data.addValue("origin", origin); 													// addition de la donne origin dans la map SQL
		List<Flight> res = new ArrayList<Flight>(); 										// creation d'une liste dans la variable "res"
		for (Map<String, Object> m : template.queryForList(selFlights, data)) {
			res.add(new Flight(new Long((Integer) m.get("id")), ((Timestamp) m.get("departure")).toLocalDateTime(),
					(String) m.get("destination"), (String) m.get("origin"), (new Long((Integer) m.get("plane"))))); // Integer a la place de longcar dans la BD c'est un Integer attendu
																													
			// addition des donnees lu dans la map dans la list<flight>
		}
		return res; // retour de la list
	}

	public void addJourney(String immat, String destination, LocalDateTime dep1, LocalDateTime dep2) { // induit un aller et un retour dans la BD
																									
		String selJourneyPlane = "SELECT id FROM plane WHERE immat LIKE :immat";            // selection de l'Id unique du plane
		MapSqlParameterSource data1 = new MapSqlParameterSource(); 	
		data1.addValue("immat", immat); 
		long id = (long)template.queryForObject(selJourneyPlane, data1, Long.class);

		insertFlight (new Flight(dep1, ownCode, destination,id));							// creation du vol aller
		insertFlight (new Flight(dep2, destination, ownCode, id));						// creation du vol retour - en mettant un id de 100000L on provoque une faute
		
	}
}