package fr.toulouse.greta.formation.data.fb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.gemfire.repository.config.EnableGemfireRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

// recherche a ce que cette classe soit dispo dans le main 
@Service("balloonService")
// 
@EnableGemfireRepositories("fr.toulouse") 

public class BalloonService {

	private BalloonRepository repo; //aggregation 
	
	@Autowired // bean qui a besoin de faire une injection 
	public void setRepo(BalloonRepository repo) {
		this.repo = repo;
	}
	
	public void execute() {
		System.out.println("Montgolfieres:");
		System.out.println(repo);
		repo.save(new Balloon(1, "Jules", 13, 2000));
		repo.save(new Balloon(2, "Vernes", 20, 1563));
		repo.save(new Balloon(3, "Cloud", 14, 1784));
		repo.save(new Balloon(4, "SkyLimit", 6, 5000));
		
		for(Balloon b:repo.findAll()) { 			// affiche tous les balloons ci-dessus FindAll est comme une liste ici 
			b.setAltitude(b.getAltitude()/3200);
			repo.save(b);
		}
		
		//for(Balloon b:repo.findAll()) 			// affiche tous les balloons ci-dessus FindAll est comme une liste ici 
		//
		//System.out.println(b);	

		//for(Balloon b:repo.findBySizeGreaterThanOrderByName(12)) 
		//System.out.println(b);
		for(Balloon b:repo.findByAltitudeGreaterThanAndAltitudeLessThanOrderBySize(0.5,0.6)) { 
		System.out.println(b);
		}
}
		
	












	private void Final() {
		// TODO Auto-generated method stub
		
	}

}
