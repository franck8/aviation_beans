package fr.toulouse.greta.formation.aspect.fb;
import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;

public class LogAspect {
	
	public void enter(JoinPoint jp) {
		System.out.println(LocalDateTime.now() + "enter" + jp.getSignature().getDeclaringTypeName()); // aspect avec greffon inside - l'aspect est un bean ici		
	}

	public void exit() {
		
		
		
		

		System.out.println(LocalDateTime.now() + "exit"); // aspect avec greffon inside - l'aspect est un bean ici
	}
}
